# environment-monitor
*Monitor for environmental conditions with data published over MQTT.*

[![Build status](https://gitlab.com/philipbarclay/environment-monitor/badges/master/build.svg)](https://gitlab.com/philipbarclay/environment-monitor/commits/master)
[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

---
Monitor of environmental conditions, primarily developed for in-home use.  Basic hardware consists of a ESP8266 microprocessor with an always connected air temperature+humidity sensor.  Also capable of monitoring up to 2 'external' temperature sensors.  The main purpose of this is to monitor the air wherever the node is placed, and also up to two temperatures some short distance from the node.  For the initial implementation these external sensors will be for monitoring the surface temperature of an exposed concrete floor.

## Hardware

*   WeMos D1 Mini
*   BME280
*   I2C OLED Screen
*   MH-z19 CO2 Sensor

| Wemos | ESP8266 | Function | Connection |
| ----- | ------- | -------- | ---------- |
| RX | GPIO3 | Serial RX | USB Serial |
| TX | GPIO1 | Serial TX | USB Serial |
| D0 | GPIO16 | | |
| D1 | GPIO5 | SCL | BME280, Screen? |
| D2 | GPIO4 | SDA | BME280, Screen? |
| D3 | GPIO0 | | |
| D4 | GPIO2 | BuiltinLED | |
| D5 | GPIO14 | 1-wire | DS18B20 |
| D6 | GPIO12 | | |
| D7 | GPIO13 | Software Serial RX | MH-Z19 |
| D8 | GPIO15 | Software Serial TX | MH-Z19 |
| A0 | ADC | Internal VCC Monitoring | |

## Software

Built using [PlatformIO](http://platformio.org/).  Upload by either clicking the arrow button, or run

```

```

Automatically installed dependencies:
*   [Homie-esp8266](https://github.com/marvinroger/homie-esp8266)
*   [Adafruit_HTU21DF_Library](https://github.com/adafruit/Adafruit_HTU21DF_Library)

The Homie framework also requires a config in JSON format in the SPIFFS of the ESP to store things like the node-name, wifi credentials etc.  A basic version is included in this repo, but the values need to be modified first.  To upload to the target, run
```
platformio run --target


MH-Z19 code from:
[Github/bertrik](https://github.com/bertrik/mhz19/blob/master/co2meter/co2meter.ino)

```
