// Copyright (c) 2017 Philip Barclay
// Released under MIT license, see LICENSE.txt for details

#include <Wire.h>
#include <SPI.h>  // Needed by the BME280 library but unused
#include <OneWire.h>
//  #include <SoftwareSerial.h>
#include "RemoteDebug.h"

#include <SparkFunBME280.h>
#include "Adafruit_SGP30.h"
#include <MHZ19.h>
#include <DallasTemperature.h>

#include <Homie.h>

// SDA=D2=GPIO4 SCL=D1=GPIO5
// DX=GPIOX

// RX=GPIO3 - RXDebug
// TX=GPIO1 - TXDebug
// D0=GPIO16
// D1=GPIO5 - SCL
// D2=GPIO4 - SDA
// D3=GPIO0
// D4=GPIO2 - BuiltinLED
// D5=GPIO14 - TXSWSerial
// D6=GPIO12 - 1-wire
// D7=GPIO13 - RXSWSerial
// D8=GPIO15 -
// A0

#define DEBUG_UART_BAUD   115200

#define LED_PIN      D4

#define SDA_PIN      D2
#define SCL_PIN      D1

#define ONEWIRE_PIN  D6

#define CO2_MHZ19_RX D7
#define CO2_MHZ19_TX D5

// #define SCREEN_I2C_ADDR 0x3c
#define TEMPHUMPRES_I2C_ADDR 0x76

#define FW_NAME              "habitatMon"
#define FW_VERSION           "0.0.2"

#define DEFAULT_TX_INTERVAL  (60)

#define HOST_NAME "esp"

char tmpBuf[64];

RemoteDebug Debug;

BME280 bme280;
Adafruit_SGP30 sgp30;
MHZ19 mhz19 = MHZ19(CO2_MHZ19_RX, CO2_MHZ19_TX);

OneWire oneWire(ONEWIRE_PIN);
DallasTemperature dallasTempDevices(&oneWire);
DeviceAddress extTemperatureAddress;

HomieNode internalTemperatureNode("temperature", "temperature");
HomieNode internalHumidityNode("humidity", "humidity");
HomieNode internalPressureNode("pressure", "pressure");
HomieNode internalAbsHumidityNode("abshumidity", "abshumidity");
HomieNode internalCO2Node("co2", "co2");
HomieNode internalVOCNode("tvoc", "tvoc");
HomieNode internalECO2Node("e-co2", "e-co2");
HomieNode externalTemperatureNode("external_temperature", "temperature");

HomieSetting<long> transmitIntervalSetting("txInterval",
  "The measurement interval of sensor readings in seconds");

unsigned long lastDataSent = 0;
unsigned long lastDataSampled = 0;

bool bme280Sensor = false;
bool extTempSensor = false;
bool co2Sensor = false;
bool vocSensor = false;

void logging(uint8_t debugLevel, const char * str)
{
  // Send via telnet to anyone listening
  if (Debug.isActive(debugLevel))
  {
    Debug.println(str);
  }

  // Also send via the serial port
  Serial.println(str);
}

/* return absolute humidity [mg/m^3] with approximation formula
* @param temperature [°C]
* @param humidity [%RH]
*/
uint32_t absoluteHumidity(float temperature, float humidity) {
    // approximation formula from Sensirion SGP30 Driver Integration ch3.15
    const float absoluteHumidity = 216.7f *
      ((humidity / 100.0f) * 6.112f *
        exp((17.62f * temperature) / (243.12f + temperature)) /
        (273.15f + temperature));  // [g/m^3]
    const uint32_t absoluteHumidityScaled =
      static_cast<uint32_t>(1000.0f * absoluteHumidity);  // [mg/m^3]
    return absoluteHumidityScaled;
}

void homieSetupHandler()
{
  internalTemperatureNode.setProperty("unit").send("C");
  internalHumidityNode.setProperty("unit").send("%");
  internalPressureNode.setProperty("unit").send("Pa");
  internalAbsHumidityNode.setProperty("unit").send("mg/m^3");

  if (co2Sensor)
  {
    internalCO2Node.setProperty("unit").send("ppm");
  }

  if (vocSensor)
  {
    internalVOCNode.setProperty("unit").send("ppb");
    internalECO2Node.setProperty("unit").send("ppm");
  }

  if (extTempSensor)
  {
    externalTemperatureNode.setProperty("unit").send("C");
  }
}

void homieLoopHandler()
{
  if (millis() - lastDataSent >= transmitIntervalSetting.get() * 1000UL ||
      lastDataSent == 0)
  {
    logging(Debug.DEBUG, "\nSensor sampling");

    /*
    if (Debug.isActive(Debug.VERBOSE)) {
      Debug.println("* This is a message of debug level VERBOSE");
    }
    if (Debug.isActive(Debug.DEBUG)) {
      Debug.println("* This is a message of debug level DEBUG");
    }
    if (Debug.isActive(Debug.INFO)) {
      Debug.println("* This is a message of debug level INFO");
    }
    if (Debug.isActive(Debug.WARNING)) {
      Debug.println("* This is a message of debug level WARNING");
    }
    if (Debug.isActive(Debug.ERROR)) {
      Debug.println("* This is a message of debug level ERROR");
    }
    */

    // Generate an LED pulse while sampling sensors
    digitalWrite(LED_PIN, LOW);

    float t = bme280.readTempC();
    // Homie.getLogger() << "Temp: " << t << " °C" << endl;
    internalTemperatureNode.setProperty("degrees").send(String(t, 2));

    float h = bme280.readFloatHumidity();
    // Homie.getLogger() << "Hum:  " << h << " %" << endl;
    internalHumidityNode.setProperty("percentage").send(String(h, 2));

    float p = bme280.readFloatPressure();
    // Homie.getLogger() << "Pres: " << p/1000.0 << " kPa" << endl;
    internalPressureNode.setProperty("pascals").send(String(p, 0));

    snprintf(tmpBuf, sizeof(tmpBuf),
      "Air sensor read, %.2f °C, %.2f %%, %.2f kPa", t, h, p);
    logging(Debug.DEBUG, tmpBuf);

    // Calculate absolute humidity.  Logged and used by the SGP30 VOC sensor
    uint32_t absHumidity = absoluteHumidity(t, h);
    snprintf(tmpBuf, sizeof(tmpBuf), "%u", absHumidity);
    internalAbsHumidityNode.setProperty("mass").send(tmpBuf);
    snprintf(tmpBuf, sizeof(tmpBuf), "Abs humidity, %u mg/m^3", absHumidity);
    logging(Debug.DEBUG, tmpBuf);

    if (extTempSensor)
    {
      dallasTempDevices.requestTemperatures();
      float t = dallasTempDevices.getTempCByIndex(0);

      // -127 is returned if the sensor is disconnected
      if (t > -100)
      {
        snprintf(tmpBuf, sizeof(tmpBuf), "Ext sensor read, %f °C", t);
        logging(Debug.DEBUG, tmpBuf);
        // Homie.getLogger() << "Ext Temp: " << t << " °C" << endl;
        externalTemperatureNode.setProperty("degrees").send(String(t, 2));
      }
      else
      {
        logging(Debug.ERROR, "Error reading external temp sensor");
      }
    }

    if (co2Sensor)
    {
      uint16_t co2 = mhz19.SampleValue();

      Homie.getLogger() << "CO2: ";
      if (co2 >= MHZ19::VALID_THRESHOLD)
      {
        snprintf(tmpBuf, sizeof(tmpBuf), "CO2 sensor read, %d ppm", co2);
        logging(Debug.DEBUG, tmpBuf);
        // Homie.getLogger() << co2 << " ppm" << endl;
        internalCO2Node.setProperty("concentration").send(String(co2));
      }
      else
      {
        switch (co2)
        {
          case MHZ19::ERROR_STARTBYTES:
            // Homie.getLogger() << "Start bytes error" << endl;
            snprintf(tmpBuf, sizeof(tmpBuf),
              "CO2 error: start bytes, expected FF 86, received %02X %02X",
              mhz19.Response[0], mhz19.Response[1]);
            logging(Debug.ERROR, tmpBuf);
            break;
          case MHZ19::ERROR_CRC:
            // Homie.getLogger() << "CRC error" << endl;
            logging(Debug.ERROR, "CO2 error: CRC");
            break;
          case MHZ19::INVALID_STATUS:
            // Homie.getLogger() << "Invalid status" << endl;
            snprintf(tmpBuf, sizeof(tmpBuf),
              "CO2 error: status, valid 40, received %02X",
              mhz19.Response[5]);
            logging(Debug.ERROR, tmpBuf);
            break;
          case MHZ19::INVALID_UUVAL:
            // Homie.getLogger() << "Invalid UU value" << endl;
            snprintf(tmpBuf, sizeof(tmpBuf),
              "CO2 error: UUVal, valid < 15k, received %d",
              (256 * (uint16_t)mhz19.Response[6]) + mhz19.Response[7]);
            logging(Debug.ERROR, tmpBuf);
            break;
          case MHZ19::ERROR_UNKNOWN:
          default:
            // Homie.getLogger() << "Unknown error: " << co2 << endl;
            logging(Debug.ERROR, "CO2 error: unknown");
            break;
        }

        /*
        snprintf(tmpBuf, sizeof(tmpBuf), "Bytes: %x %x %x %x %x %x %x %x %x",
          mhz19.rsp[0], mhz19.rsp[1], mhz19.rsp[2],
          mhz19.rsp[3], mhz19.rsp[4], mhz19.rsp[5],
          mhz19.rsp[6], mhz19.rsp[7], mhz19.rsp[8]);
        logging(Debug.DEBUG, tmpBuf);
        */
      }
    }

    if (vocSensor)
    {
      uint16_t tVoc = sgp30.TVOC;
      snprintf(tmpBuf, sizeof(tmpBuf), "tVOC sensor read, %d ppb", tVoc);
      logging(Debug.DEBUG, tmpBuf);

      uint16_t eCO2 = sgp30.eCO2;
      snprintf(tmpBuf, sizeof(tmpBuf), "eCO2 sensor read, %d ppm", eCO2);
      logging(Debug.DEBUG, tmpBuf);

      // At startup the sensor returns CO2 of 400, so dont send results
      // until valid data arrives
      if (eCO2 > 400)
      {
        internalVOCNode.setProperty("concentration").send(String(tVoc));
        internalECO2Node.setProperty("concentration").send(String(eCO2));
      }
      // Also update the VOC sensor with the latest temperature and humidity
      sgp30.setHumidity(absHumidity);
    }

    lastDataSent += transmitIntervalSetting.get() * 1000UL;
    digitalWrite(LED_PIN, HIGH);
  }
}

void onHomieEvent(const HomieEvent& event)
{
  switch (event.type)
  {
    case HomieEventType::WIFI_CONNECTED:
      Homie.getLogger() << F("WiFi Connected") << endl;
      Homie.getLogger() << F("  IP: ") << event.ip << endl;
      Homie.getLogger() << F("  gateway: ") << event.gateway << endl;
      Homie.getLogger() << F("  mask: ") << event.mask << endl;
      Homie.getLogger() << F("Starting telnet") << endl;
      MDNS.addService("telnet", "tcp", 23);
      Debug.begin(HOST_NAME);
      Homie.getLogger() << F("  hostname: ") << HOST_NAME << endl;
      Debug.setResetCmdEnabled(true);
      Debug.showTime(true);
      Debug.showColors(true);
      break;
    default:
      break;
  }
}

void setup()
{
  Wire.begin(SDA_PIN, SCL_PIN);  // SDA=D2=GPIO4 SCL=D1=GPIO5

  // Debug serial over the USB connection
  Serial.begin(DEBUG_UART_BAUD);
  // Serial.setDebugOutput(true);
  delay(4000);  // Debug delay

  // Force a disconnect as a workaround for setup hangs.
  WiFi.disconnect();

  Serial.println();
  Serial.println();

  Serial.println(F("HABITAT MONITOR"));
  Serial.println(F("Philip Barclay"));
  Serial.println();

  Serial.println(F("ESP8266 Info:"));
  Serial.print(F("  Chip ID: "));
  Serial.println(ESP.getChipId(), HEX);
  Serial.print(F("  Reset Reason: "));
  Serial.println(ESP.getResetReason());

  Serial.print(F("  Flash chip ID: "));
  Serial.println(ESP.getFlashChipId(), HEX);
  Serial.print(F("  Flash chip real size (bytes): "));
  Serial.println(ESP.getFlashChipRealSize());
  Serial.print(F("  Flash chip apparent size (bytes): "));
  Serial.println(ESP.getFlashChipSize());
  Serial.print(F("  Flash chip speed (MHz): "));
  Serial.println(ESP.getFlashChipSpeed() / 1e6);

  // Configure BME280 sensor
  bme280.settings.commInterface = I2C_MODE;
  bme280.settings.I2CAddress = TEMPHUMPRES_I2C_ADDR;

  // runMode can be:
  //  0, Sleep mode
  //  1 or 2, Forced mode
  //  3, Normal mode
  bme280.settings.runMode = 1;

  // tStandby can be:
  //  0, 0.5ms
  //  1, 62.5ms
  //  2, 125ms
  //  3, 250ms
  //  4, 500ms
  //  5, 1000ms
  //  6, 10ms
  //  7, 20ms
  bme280.settings.tStandby = 5;

  // filter can be off or number of FIR coefficients to use:
  //  0, filter off
  //  1, coefficients = 2
  //  2, coefficients = 4
  //  3, coefficients = 8
  //  4, coefficients = 16
  bme280.settings.filter = 0;

  // [temp,pres,humid]OverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  bme280.settings.tempOverSample = 1;
  bme280.settings.pressOverSample = 1;
  bme280.settings.humidOverSample = 1;

  Serial.println(F("Init BME280 sensor:"));
  uint8_t id = bme280.begin();
  if (id != 0x60)
  {
    Serial.println(F("  Did not initialize!"));
    Serial.print(F("  ID returned: "));
    Serial.println(id, HEX);
  }
  else
  {
    Serial.println(F("  detected"));
  }

  delay(10);  // Wait a bit to allow sensor to be active
  Serial.print(F("  temp: "));
  Serial.print(bme280.readTempC());
  Serial.println(F(" °C"));
  Serial.print(F("  hum: "));
  Serial.print(bme280.readFloatHumidity());
  Serial.println(F(" %"));
  Serial.print(F("  pres: "));
  Serial.print(bme280.readFloatPressure());
  Serial.println(F(" Pa"));

  // Search for optional SGP30 VOC sensor
  Serial.println(F("Search SGP30 VOC sensor:"));
  vocSensor = sgp30.begin();
  if (vocSensor)
  {
    Serial.println(F("  detected"));

    Serial.print(F("  serial #"));
    Serial.print(sgp30.serialnumber[0], HEX);
    Serial.print(sgp30.serialnumber[1], HEX);
    Serial.println(sgp30.serialnumber[2], HEX);
  }
  else
  {
    Serial.println(F("  not detected"));
  }

  // Search for optional MH-z19 CO2 sensor via UART
  // Need to add code here to detect if sensor present or not
  Serial.println(F("Search MH-z19 CO2 sensor:"));
  co2Sensor = mhz19.Begin();
  if (co2Sensor)
  {
    Serial.println(F("  detected"));
  }
  else
  {
    Serial.println(F("  not detected"));
  }

  // Search for optional single external 1-wire temperature sensor.
  Serial.println(F("Search 18B20 temperature sensor:"));
  // locate devices on the bus
  dallasTempDevices.begin();
  int extDevCount = dallasTempDevices.getDeviceCount();
  Serial.print(F("  detected "));
  Serial.print(extDevCount, DEC);
  Serial.println(F(" devices"));
  if (extDevCount > 0)
  {
    extTempSensor = true;
  }

  if (extTempSensor)
  {
    dallasTempDevices.getAddress(extTemperatureAddress, 0);
    Serial.print(F("  address: "));
    for (uint8_t i = 0; i < 8; i++)
    {
      if (extTemperatureAddress[i] < 16)
      {
        Serial.print("0");
      }
      Serial.print(extTemperatureAddress[i], HEX);
    }
    Serial.println(F(""));

    dallasTempDevices.requestTemperatures();
    float temp = dallasTempDevices.getTempCByIndex(0);
    Serial.print(F("  temp: "));
    Serial.print(temp);
    Serial.println(F(" °C"));
  }

  Homie_setFirmware(FW_NAME, FW_VERSION);

  Homie.setSetupFunction(homieSetupHandler);
  Homie.setLoopFunction(homieLoopHandler);
  Homie.onEvent(onHomieEvent);
  Homie.disableResetTrigger();
  Homie.setLedPin(LED_PIN, LOW);

  internalTemperatureNode.advertise("units");
  internalTemperatureNode.advertise("degrees");

  internalHumidityNode.advertise("units");
  internalHumidityNode.advertise("percentage");

  internalPressureNode.advertise("units");
  internalPressureNode.advertise("pascals");

  if (extTempSensor)
  {
    externalTemperatureNode.advertise("units");
    externalTemperatureNode.advertise("degrees");
  }

  if (co2Sensor)
  {
    internalCO2Node.advertise("units");
    internalCO2Node.advertise("concentration");
  }

  if (vocSensor)
  {
    internalVOCNode.advertise("units");
    internalVOCNode.advertise("concentration");
    internalECO2Node.advertise("units");
    internalECO2Node.advertise("concentration");
  }

  transmitIntervalSetting.setDefaultValue(DEFAULT_TX_INTERVAL)
    .setValidator([] (long candidate)
  {
    return candidate > 0;
  });

  Homie.setup();

  // Seed the timer loops
  lastDataSent = millis();
  lastDataSampled = millis();
}

void loop()
{
  Homie.loop();
  Debug.handle();

  // 1 second interval for sampling the VOC sensor to keep its baseline correct
  if (millis() - lastDataSampled >= 1000UL)
  {
    if (!sgp30.IAQmeasure())
    {
      logging(Debug.ERROR, "SGP30 error: measure failed");
    }

    lastDataSampled += 1000UL;
  }
}



/*
ESP.getResetReason() returns String containing the last reset resaon in human readable format.
ESP.getFreeHeap() returns the free heap size.
ESP.getChipId() returns the ESP8266 chip ID as a 32-bit integer.
ESP.getCycleCount() returns the cpu instruction cycle count since start as an unsigned 32-bit. This is useful for accurate timing of very short actions like bit banging.

Several APIs may be used to get flash chip info:
ESP.getFlashChipId() returns the flash chip ID as a 32-bit integer.
ESP.getFlashChipSize() returns the flash chip size, in bytes, as seen by the SDK (may be less than actual size).
ESP.getFlashChipRealSize() returns the real chip size, in bytes, based on the flash chip ID.
ESP.getFlashChipSpeed(void) returns the flash chip frequency, in Hz.
*/

/*
Serial.println("Flash chip stats:");
Serial.print("  id: ");
Serial.println(ESP.getFlashChipId()); // returns the flash chip ID as a 32-bit integer.
Serial.print("  size: ");
Serial.println(ESP.getFlashChipSize());
Serial.print("  realsize: ");
Serial.println(ESP.getFlashChipRealSize());
Serial.print("  speed: ");
Serial.println(ESP.getFlashChipSpeed());
*/
