EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MicroModules
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L WemosD1Mini U?
U 1 1 58CB52CC
P 7900 3100
F 0 "U?" H 7800 3725 50  0000 R CNN
F 1 "WemosD1Mini" H 7800 3650 50  0000 R CNN
F 2 "" H 7950 2450 50  0001 L CNN
F 3 "" H 8150 3700 50  0001 C CNN
	1    7900 3100
	1    0    0    -1  
$EndComp
$Comp
L ADS1115 U?
U 1 1 58D23E51
P 5200 5750
F 0 "U?" H 5350 6075 50  0000 R CNN
F 1 "ADS1115" H 5600 5400 50  0000 R CNN
F 2 "" H 5250 5100 50  0001 L CNN
F 3 "" H 5350 6150 50  0001 C CNN
	1    5200 5750
	1    0    0    -1  
$EndComp
Text Label 5700 5550 0    60   ~ 0
SCL
Text Label 5700 5650 0    60   ~ 0
SDA
$Comp
L USB_OTG J?
U 1 1 58D24817
P 1350 1350
F 0 "J?" H 1150 1800 50  0000 L CNN
F 1 "USB_OTG" H 1150 1700 50  0000 L CNN
F 2 "" H 1500 1300 50  0001 C CNN
F 3 "" H 1500 1300 50  0001 C CNN
	1    1350 1350
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 58D24A2C
P 1800 1050
F 0 "#PWR?" H 1800 900 50  0001 C CNN
F 1 "VCC" H 1800 1200 50  0000 C CNN
F 2 "" H 1800 1050 50  0001 C CNN
F 3 "" H 1800 1050 50  0001 C CNN
	1    1800 1050
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 58D24B77
P 7800 2400
F 0 "#PWR?" H 7800 2250 50  0001 C CNN
F 1 "VCC" H 7800 2550 50  0000 C CNN
F 2 "" H 7800 2400 50  0001 C CNN
F 3 "" H 7800 2400 50  0001 C CNN
	1    7800 2400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR?
U 1 1 58D24B94
P 7900 2250
F 0 "#PWR?" H 7900 2100 50  0001 C CNN
F 1 "+3.3V" H 7900 2390 50  0000 C CNN
F 2 "" H 7900 2250 50  0001 C CNN
F 3 "" H 7900 2250 50  0001 C CNN
	1    7900 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 5550 5950 5550
Wire Wire Line
	5550 5650 5950 5650
Wire Wire Line
	1650 1150 1800 1150
Wire Wire Line
	1800 1150 1800 1050
Wire Wire Line
	1350 1750 1350 1950
Wire Wire Line
	7800 2400 7800 2500
Wire Wire Line
	7900 2250 7900 2500
$EndSCHEMATC
